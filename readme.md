## Connect X

Connect X is a tic-tac-toe X Connect 4 mashup game.

To Run:

1. Clone the repo
2. Run `npm install` in directory containing `package.json`
3. Run `npm run dev` in the directory containing `package.json`

Current Implementation:
    
- Defaults to a 4x4 board
- Win condition: Get 3 board positions in a row to win
    - Vertical or Horizontal (Diagonal not yet implemented)

Vision:
    
- To improve the game to allow the user to select board size 
- To let the user select the win condition "in a row" count