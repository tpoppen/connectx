export const TILE_CLICK = "TILE_CLICK";

export function tileClick(tileKey, player) {
    return {
        type: TILE_CLICK,
        payload: {
            tileKey: tileKey,
            player: player
        }
    };
}