export const RESTART_CLICK = 'RESTART_CLICK';

export function restartClick() {
    return {
        type: RESTART_CLICK
    }
}