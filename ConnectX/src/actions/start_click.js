export const START_CLICK = 'START_CLICK';

export function startClick() {
    return {
        type: START_CLICK
    }
}