import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { restartClick } from '../actions/restart_click';

class GameOver extends Component {
    render() {
        if (this.props.winner !== 0) {
            return (
                <div className="game-over-container" >
                    <div>
                        Player { this.props.winner } wins!
                    </div>
                    <span 
                        className="btn btn-primary"
                        onClick={() => this.props.restartClick() }>
                        Restart
                    </span>
                </div>
            );
        } else {
            return (
                <div></div>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        winner: state.gameState.winner
    }
}

function mapStateToDispatch(dispatch) {
    return bindActionCreators({ restartClick }, dispatch);
}

export default connect(mapStateToProps, mapStateToDispatch)(GameOver);