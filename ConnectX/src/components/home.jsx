import React from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Board from './board';
import GameOver from './game_over';

const Home = ({ turn }) => {
  return (
    <div>
      <div className="game-title">
        <h2>Connect X</h2>
      </div>
      <div className="score-board">
        <div className="player-detail">
          <h4 className="player-1-detail">Player 1</h4>
            { turn === 1 ? <h6>Your Turn</h6> : <h6></h6> }
        </div>
        <div className="player-detail">
          <h4 className="player-2-detail">Player 2</h4> 
          { turn === 2 ? <h6>Your Turn</h6> : <h6></h6> }
        </div>
      </div>
      <div id="game-board">
        <Board />
      </div>      
      <div>
        <GameOver />
      </div>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    turn: state.turn
  }
}

export default connect(mapStateToProps)(Home);