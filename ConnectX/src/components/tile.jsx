import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { tileClick } from '../actions/tile_click';

class Tile extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var tileText = '';
        var classes = 'tile btn';
        if (this.props.player == 1) {
            tileText = 'X';
            classes += ' btn-primary';
        } else if (this.props.player == 2) {
            tileText = 'O';
            classes += ' btn-danger';
        }
        return (
            <span 
                className={ classes }
                onClick={ () => {
                    if (this.props.player === 0) {
                        this.props.tileClick(this.props.tileID, this.props.turn);
                    }
                } }>
                { tileText }
            </span>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        ...ownProps,
        turn: state.turn
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ tileClick: tileClick }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Tile);