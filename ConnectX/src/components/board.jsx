import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Tile from './tile';
import winConditionChecker from '../helpers/win_condition_checker';

class Board extends Component {
    // Render Helpers
    row(tileRow, rowNum) {
        return (
            <div 
                key={'tile-row' + rowNum} 
                className="board-row">
                { tileRow.map((tile) => 
                        <Tile 
                            key={ tile.key.toString() } 
                            tileID={ tile.key.toString() } 
                            player={tile.player} 
                        />) }
            </div>
        )
    }

    table() {
        var tilesArray = [];
        var n = 1;
        for(var i = 0; i < this.props.boardSize.rows; i++) {
            tilesArray[i] = [];
            for(var j = 0; j < this.props.boardSize.columns; j++) {
                tilesArray[i][j] = this.props.tiles[n];
                n++;
            }
        }
        var rowNum = 0;
        return (
            <div className="board">
                { tilesArray.map((tileRow) => this.row(tileRow, rowNum++)) }
            </div>
        );
    }

    // Render our game board
    render() {
        return (
            <div className="board-container">
                { this.table() }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        boardSize: state.boardSize,
        tiles: state.gameState.tiles,
    }
}

export default connect(mapStateToProps)(Board);