export default function(rows, columns, tiles) { 
    var player1 = 1;
    var player2 = 2;
    var totalTiles = rows * columns;
    for(var i = 1; i <= totalTiles; i++) {
        var column = i % rows;
        var row = (i - column) / rows;
        if (checkDown(tiles, row, column, rows, columns, player1) >= 3 || 
            checkRight(tiles, row, column, rows, columns, player1) >= 3) {
            return 1; // player 1 wins
        }
        
        if (checkDown(tiles, row, column, rows, columns, player2) >= 3 || 
        checkRight(tiles, row, column, rows, columns, player2) >= 3) {
            return 2; // player 2 wins
        }
    }

    // return 0 if no one wins
    return 0;
}

const WIN_AMOUNT = 3;
function checkDown(tiles, x, y, maxRow, maxCol, player) {
    var index = x * maxRow + y;
    var matchingTile = tiles[index.toString()];
    if (matchingTile.player !== player) {
        return 0;
    }

    var downCount = 1;
    var nextIndex = (x+1) * maxRow + y;
    if (nextIndex <= maxRow * maxCol) {
        downCount += checkDown(tiles, x+1, y, maxRow, maxCol, player);
    }
    return downCount;
}

function checkRight(tiles, x, y, maxRow, maxCol, player) {
    var index = x * maxRow + y;
    var matchingTile = tiles[index.toString()];
    if (matchingTile.player !== player) {
        return 0;
    }

    var rightCount = 1;
    var nextIndex = x * maxRow + y + 1;
    if (nextIndex <= maxRow * maxCol) {
        rightCount += checkRight(tiles, x, y+1, maxRow, maxCol, player);
    }
    return rightCount;
}


// todo: Check diagonals
function checkUpLeft(tiles, x, y, maxRow, maxCol, player) {

}

function checkUpRight(tiles, x, y, maxRow, maxCol, player) {

}

function checkDownLeft(tiles, x, y, maxRow, maxCol, player) {

}

function checkDownRight(tiles, x, y, maxRow, maxCol, player) {

}
