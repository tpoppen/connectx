import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import Home from './components/home';
import rootReducer from './reducers/root_reducer';

import './styles/main.css';

const appStore = createStore(rootReducer);
ReactDOM.render(
    <Provider store={ appStore }>
        <Home /> 
    </Provider>, 
    document.getElementById("root-container")
);