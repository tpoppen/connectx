import { DEFAULT_ROWS, DEFAULT_COLS, CreateTiles } from './state_defaults';
import { TILE_CLICK } from '../actions/tile_click';
import { RESTART_CLICK } from '../actions/restart_click';

import winConditionChecker from '../helpers/win_condition_checker';

export default function(state, action) {
    var newTiles;
    if (!state) {
        newTiles = tiles(state, action);
    } else {
        newTiles = tiles(state.tiles, action);
    }

    var gameWinner = winner(DEFAULT_ROWS, DEFAULT_COLS, newTiles);
    console.log(gameWinner);
    return {
        tiles: newTiles,
        winner: gameWinner
    };
};

function tiles(state = CreateTiles(DEFAULT_ROWS, DEFAULT_COLS), action) {
    switch(action.type) {
    case TILE_CLICK:
        var newTile = {
            key: action.payload.tileKey,
            player: action.payload.player
        };
        // return our new game board
        return { ...state, [action.payload.tileKey]: newTile };
    case RESTART_CLICK:
        // todo: when rows and cols are configurable, use selected vals instead of defaults
        return CreateTiles(DEFAULT_ROWS, DEFAULT_COLS);
    default:
        return state;
    }
};

function winner(rows, columns, tiles) {
    return winConditionChecker(rows, columns, tiles);
};