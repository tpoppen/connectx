import { TILE_CLICK } from '../actions/tile_click';

export default function(state = 1, action) {
    switch(action.type) {
    case TILE_CLICK:
        if (action.payload.player === 1) {
            return 2;
        } else if (action.payload.player === 2) {
            return 1;
        }
    default:
        return state;
    }
}