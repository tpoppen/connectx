import { combineReducers } from 'redux';
import BoardSize from './board_size';
import GameStateReducer from './game_state';
import TurnReducer from './turn';

const rootReducer = combineReducers({
    boardSize: BoardSize,
    gameState: GameStateReducer,
    turn: TurnReducer,
});

export default rootReducer;