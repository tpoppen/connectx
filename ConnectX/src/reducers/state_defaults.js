import _ from 'lodash';

export const DEFAULT_ROWS = 4;
export const DEFAULT_COLS = 4;

export const DEFAULT_BOARD_SIZE = {
    rows: DEFAULT_ROWS,
    columns: DEFAULT_COLS
}

export function CreateTiles(rows, columns) {
    // create our base game board of tiles each with a unique identifier
    var totalTiles = rows * columns;
    var tiles = [];
    for(var i = 1; i <= totalTiles; i++) {
        tiles[i] = {
            key: i,
            player: 0,
        }
    }
    var boardObject = _.mapKeys(tiles, 'key');
    return boardObject;
}