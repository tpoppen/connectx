import { DEFAULT_BOARD_SIZE } from './state_defaults';

export default function(state = DEFAULT_BOARD_SIZE, action) {
    // TODO : eventually will add an action to increase board size, for now just default size of 4x4
    switch(action.type) {
    default:
        return state;
    }
}